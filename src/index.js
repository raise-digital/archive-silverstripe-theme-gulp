'use strict';

module.exports = function(localConfig) {
  module.config = localConfig;
  const config = require('./config.js');

  const autoprefixer = require('autoprefixer');
  const browserSync = require('browser-sync');
  const changed = require('gulp-changed');
  const del = require('del');
  const getWebpackConfig = require('./util/getWebpackConfig');
  const gulp = require('gulp');
  const gulpif = require('gulp-if');
  const handleErrors = require('./util/handleErrors');
  const htmlmin = require('gulp-htmlmin');
  const logger = require('./util/bundleLogger');
  const merge = require('merge2');
  const nano = require('cssnano');
  const postcss = require('gulp-postcss');
  const runSequence = require('run-sequence');
  const sass = require('gulp-sass');
  const sassLint = require('gulp-sass-lint');
  const sizereport = require('gulp-sizereport');
  const sourcemaps = require('gulp-sourcemaps');
  const svgo = require('gulp-svgo');
  const ts = require('gulp-typescript');
  const tsProject = ts.createProject({'target': 'es5'});
  const watch = require('gulp-watch');
  const webpack = require('webpack');
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const webpackHotMiddleware = require('webpack-hot-middleware');

  const removeClasses = require('./util/removeCssClasses')(config.sass.remove);
  const processors = [removeClasses, autoprefixer({ browsers: config.sass.prefix })];


  gulp.task('assets', callback => {
    if (config.assets.length <= 0) {
      return callback();
    }

    config.assets.forEach(entry => {
      gulp.src(entry.src).pipe(gulp.dest(entry.dest));
    });

    return callback();
  });

  function browserSyncTask() {
    const wpConfig = getWebpackConfig('watch');
    const bundler = webpack(wpConfig);

    config.browserSync.middleware = [
      webpackDevMiddleware(bundler, {
        publicPath: wpConfig.output.publicPath,
        stats: 'minimal',
        hot: true,
      }),
      webpackHotMiddleware(bundler),
    ];

    browserSync.init(config.browserSync);
  }

  function registerBrowserSync() {
    gulp.task('browserSync', browserSyncTask);
  }

  gulp.task('clean', callback => {
    del(config.dest, { dot: true, force: true }).then(() => {
      del(config.ts.dest, { dot: true, force: true }).then(() => {
        callback();
      });
    });
  });

  gulp.task('html', () => {
    const isProd = global.env === 'prod';

    return gulp
      .src(config.html.src)
      .pipe(changed(config.html.dest))
      .pipe(gulpif(isProd, htmlmin(config.html.compression)))
      .pipe(gulp.dest(config.html.dest))
      .pipe(browserSync.reload({ stream: true }));
  });

  gulp.task('images', () => {
    return gulp
      .src(config.images.src)
      .pipe(changed(config.images.dest))
      .pipe(svgo({ plugins: [{ removeViewBox: false }, { cleanupIDs: false }] }))
      .pipe(gulp.dest(config.images.dest));
  });

  gulp.task('report', () => {
    let src = config.report.src;

    if (config.assets && config.assets.length) {
      config.assets.forEach(el => src.push(el.dest));
    }

    src = src.map(el => `${el}/**/*`);

    return gulp.src(src).pipe(sizereport({ gzip: true }));
  });

  gulp.task('sass', () => {
    const isProd = global.env === 'prod';
    if (isProd) {
      processors.push(nano(config.sass.compression));
    }

    return (
      gulp
        .src(config.sass.src)
        // Linting
        .pipe(sassLint())
        .pipe(sassLint.format())
        .pipe(sassLint.failOnError())
        .on('error', handleErrors)

        // Sourcemaps if not prod
        .pipe(gulpif(!isProd, sourcemaps.init()))

        // Copilation
        .pipe(sass(config.sass.options))
        .on('error', handleErrors)

        // Post processing
        .pipe(postcss(processors))

        // Sourcemaps if not prod
        .pipe(gulpif(!isProd, sourcemaps.write()))

        // Dest & reloading
        .pipe(gulp.dest(config.sass.dest))
        .pipe(gulpif(!isProd, browserSync.reload({ stream: true })))
    );
  });

  gulp.task('ts', function() {
      return gulp.src(config.ts.src)
          .pipe(tsProject())
          .pipe(sourcemaps.write())
          .pipe(gulp.dest(config.ts.dest));
  });

  gulp.task('webpack', cb => {
    const config = getWebpackConfig(global.env);
    webpack(config, (err, stats) => {
      logger(err, stats);
      cb();
    });
  });

  gulp.task('default', ['clean'], callback => {
    global.env = global.env || 'build';

    const tasks = ['sass', 'html', 'images', 'assets', 'ts'];

    if (global.env === 'build') {
      tasks.push('webpack');
    }

    runSequence(...tasks, callback);
  });

  gulp.task('production', callback => {
    global.env = 'prod';
    runSequence('default', 'webpack', 'report', callback);
  });

  gulp.task('watch', callback => {
    // Set environment
    global.env = 'watch';
    registerBrowserSync()

    watch(config.sass.src, () => {
      runSequence('sass');
    });

    watch(config.images.src, () => {
      runSequence('images', browserSync.reload);
    });

    watch(config.html.src, () => {
      runSequence('html', browserSync.reload);
    });

    watch(config.ts.src, () => {
      runSequence('ts');
    });

    runSequence('default', 'browserSync', callback);
  });
}
