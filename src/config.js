const config = module.parent.config;

const dir = module.parent.config.dir;
const dest = module.parent.config.dest;
const src = module.parent.config.src;

const assets = (typeof module.parent.config.assets === 'undefined') ? [] : module.parent.config.assets; 

const browserSync = {
  port: module.parent.config.port,
  proxy: `http://${module.parent.config.proxy}`,
  notify: false,
  open: false,
};

const html = {
  src: `${src}/templates/**/*.*`,
  dest: `${dest}/templates`,

  compression: {
    collapseWhitespace: true,
  },
};

const images = {
  src: `${src}/images/**`,
  dest: `${dest}/images`,
};

const sass = {
  src: `${src}/sass/**/*.{sass,scss}`,
  dest: `${dest}/css`,
  options: {
    outputStyle: 'expanded',
    indentedSyntax: true,
    includePaths: module.parent.config.sassIncludePaths,
  },

  prefix: 'last 3 versions',

  remove: [],

  compression: {
    safe: true,
    core: true,
    autoprefixer: false,
    discardComments: {
      removeAll: true,
    },
  },
};

const ts = {
  src: `${src}/typescript/**/*.ts`,
  dest: `${dir}/build`,
}

const js = {
  context: ts.dest,
  entry: module.parent.config.jsEntries,
  output: {
    filename: '[name].js',
    path: `${dest}/javascript`,
    publicPath: `/resources/themes/${module.parent.config.theme}/javascript`,
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: [
          {
            loader: 'babel-loader',
          },
        ],
      },
    ],
  },
};

const report = {
  src: [sass.dest, js.output.path, images.dest],
};

module.exports = {
  dir,
  dest,
  src,
  browserSync,
  sass,
  assets,
  images,
  html,
  report,
  js,
  ts,
};
